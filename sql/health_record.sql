create database health_record character set utf8mb4;

create table health_doctor (
    id bigint(32) not null auto_increment comment '编号',
    mobile varchar(32) not null comment '手机号',
    password varchar(256) comment '密码',
    salt varchar(256) comment '盐',
    openid varchar(256) comment 'openid',
    nick_name varchar(1024) comment '昵称',
    token varchar(256) comment 'token',
    clinic_name varchar(1024) comment '诊所名称',
    x varchar(32) comment '纬度',
    y varchar(32) comment '经度',
    address varchar(1024) comment '地址',
    identity varchar(32) comment '身份证号',
    certificate0 varchar(256) comment '职业医师证正面',
    certificate1 varchar(256) comment '职业医师证反面',
    `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
    `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
    `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
    `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
    primary key(id)
) comment = '医生表';

create table health_patient (
    id bigint(32) not null auto_increment comment '编号',
    mobile varchar(32) not null comment '手机号',
    password varchar(256) comment '密码',
    salt varchar(256) comment '盐',
    openid varchar(256) comment 'openid',
    nick_name varchar(1024) comment '昵称',
    address varchar(1024) comment '地址',
    sex varchar(1) comment '性别（0：女，1：男）',
    age int comment '年龄',
    revisit_time datetime(0) comment '回访时间',
    doctor_id bigint(32) not null,
    `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
    `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
    `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
    `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
    primary key(id)
) comment = '病人表';

create table health_record (
    id bigint(32) not null auto_increment comment '编号',
    doctor_id bigint(32) not null,
    patient_id bigint(32) not null,
    `diagnosis_result` varchar(1024) DEFAULT NULL comment '诊断结果',
    method varchar(1024) comment '治疗方法',
    drug varchar(1024) comment '用药',
    `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
    `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
    `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
    `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
    primary key(id)
) comment ='就诊记录表';

create table health_record_file (
    id bigint(32) not null auto_increment comment '编号',
    record_id bigint(32) not null,
    path varchar(256) comment '文件路径',
    url varchar(256)  comment 'url',
    primary key(id)
) comment ='就诊文件表';


