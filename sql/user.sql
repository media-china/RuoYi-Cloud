DROP TABLE IF EXISTS `biz_user`;
create table biz_user (
    id bigint(32) not null auto_increment comment '编号',
    mobile varchar(32) not null comment '手机号',
    code varchar(32) comment '验证码',
    password varchar(256) comment '密码',
    salt varchar(256) comment '盐',
    openid varchar(256) comment 'openid',
    nick_name varchar(1024) comment '昵称',
    x varchar(32) comment '纬度',
    y varchar(32) comment '经度',
	token varchar(256) comment 'token',
    `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
    `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
    `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
    `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
    `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
    primary key(id)
) comment = '用户表';

DROP TABLE IF EXISTS `biz_relation`;
create table biz_relation (
    id bigint(32) not null auto_increment comment '编号',
    my_id bigint(32) comment '用户',
    my_mobile varchar(32) comment '手机号',
    you_id bigint(32) comment '联系人用户',
    you_mobile varchar(32) comment '联系人手机号',
    type varchar(1) comment '关系类型（0亲人 1朋友）',
    status varchar(1) default '0' comment '状态（0正常 1消息免打扰 2拉黑）',
    locate_status varchar(1) default '0' comment '位置状态（0正常 1对其隐身 2靠近消失）',
    `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
    `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
    `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
    `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
    `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
    primary key(id)
) comment = '用户关系表';

DROP TABLE IF EXISTS `biz_invite`;
create table biz_invite (
    id bigint(32) not null auto_increment comment '编号',
    my_id bigint(32) comment '邀请用户',
    my_mobile varchar(32) comment '邀请手机号',
    you_id bigint(32) comment '被邀请用户',
    you_mobile varchar(32) comment '被邀请手机号',
    status varchar(1) comment '状态（0待接受 1同意 2拒绝）',
    `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
    `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
    `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
    `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
    `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
    primary key(id)
) comment = '邀请表';

DROP TABLE IF EXISTS `biz_version`;
CREATE TABLE `biz_version` (
    `id` bigint(32) NOT NULL AUTO_INCREMENT comment '编号',
    `device_type` varchar(32) NOT NULL comment '设备类型（00安卓 01iOS）',
    `version` varchar(256) NOT NULL comment '版本号',
    `description` varchar(256) NOT NULL comment '描述',
    `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
    `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
    `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
    `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
    `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT = '版本表';

INSERT INTO `biz_version` (id, `device_type`,`version`,`description`, `create_time`) VALUES (1, '00','1.0.1','初始版本', current_timestamp);

drop table if exists `biz_path`;
create table `biz_path` (
    `path_id` bigint(32) NOT NULL AUTO_INCREMENT comment '路线编号',
    `user_id` int(11) NOT NULL COMMENT '用户ID',
    `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
    `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
    `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
    `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`path_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT = '用户路线表';

drop table if exists `biz_path_location`;
create table `biz_path_location` (
    `id` bigint(64) NOT NULL AUTO_INCREMENT comment '路线位置编号',
    `path_id` bigint(32) NOT NULL comment '路线编号',
    x varchar(32) comment '纬度',
    y varchar(32) comment '经度',
    `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
    `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
    `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
    `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT = '用户路线表';

drop table if exists `sys_file`;
create table `sys_file` (
    `id` bigint(64) NOT NULL AUTO_INCREMENT comment '文件id',
    `name` varchar(1024) NOT NULL comment '名称',
    `url` varchar(1024) NOT NULL comment '地址',
    description varchar(1024) comment '描述',
    parent_id bigint(64) default '-1' comment '首页视频id',
    _order int default '0' comment '序号',
    `is_vip` int(1) default 1 comment '是否vip（0是 1否）',
    user_id bigint(64) comment '所属用户',
    `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
    `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
    `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
    `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
    remark varchar(500)  default '' comment '备注信息',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT = '文件表';