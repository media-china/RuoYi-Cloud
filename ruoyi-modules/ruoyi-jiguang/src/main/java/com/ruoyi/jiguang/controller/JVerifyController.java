package com.ruoyi.jiguang.controller;

import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpRequest;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/jverify")
public class JVerifyController {

    // 私钥
    public static final String prikey = "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAOgxyBywu0axkFXY" +
            "S81+7eVbqgm4SALF581lZw/JuGh95sICQuFwdzOnVYPmnaVqiZPWzhJrN31kI03w" +
            "WwBSmyCegh37NUxxFl6K8lxwIY/cn7Tkz0WXeTjVCEEF1GVuqxXV77DZKLykB5F9" +
            "zIpjeTTkaJ8kVBelJjbZqD+V7qMzAgMBAAECgYAwO9ckDpp4rv4atxnYEFv+3zHN" +
            "XximatIiWsQ0BWVnX3AGMU4PYruAYWxkQA3ThPQbX+3i6Z7XT2v+DsX4pTBQCwY6" +
            "wtyR3mhhjZxkKZl13J/VZ0+bgFaCPpXuLQu5J/aKkZxBhR2Nvl4KSMj2xf82m2OS" +
            "/lzgvosf01dWHt3XwQJBAPtWuA9uLk09cShUqmUcU1qoFpq+r6w2yZtR3IV7E/WK" +
            "mpxJqRn7D+AtGXvUUHWBWy1AVyFM4n5xjgQXbCQS0IMCQQDsgCve7acNup2eOey/" +
            "9hcflu0+zQaYOFgwRP014uLClOgi16bqyK4odbQqYAibYWfdGct95aPhi+KXhxhz" +
            "xwORAkEAwRl0Gi7NlfxBpvm9XCdyBvGjREqCj24cYJ95LHhN8lUFylNxfwt7vAEK" +
            "Vi/djRnQIikPh/8Y+Ipn0M7p/6EQ3wJBAIdOgUsG3redGAZpj4j4C5y4Jb3zYR1/" +
            "xvy+y7ujtiarOPCOPuI+tF1TkiNYVDRJkznNQz4hPxSQirA0y4mZx/ECQHIi6BUA" +
            "az8Dw9TtXd0mKpZyHTxG09yJSaxKQ8JlrCz3L1+saitvVmgW2Mdot0aaJ8RzP4Yd" +
            "O25z6sK/tLSrAZY=";

    @RequestMapping("/getPhone")
    @ResponseBody
    public String getPhone(String loginToken) throws InvalidKeyException, NoSuchPaddingException, NoSuchAlgorithmException, BadPaddingException, IllegalBlockSizeException, InvalidKeySpecException {

        String host = "https://api.verification.jpush.cn/v1/web/loginTokenVerify";
        String appKey = "2472cded1b1cdeb935fabff9";
        String Secret = "0df3b685ebd8092166905e0f";
        Map<String, Object> map = new HashMap<>();
        map.put("loginToken", loginToken);
        map.put("exID", null);

        String result = HttpRequest.post(host).header("Authorization", "Basic " + Base64.getUrlEncoder().
                encodeToString((appKey + ":" + Secret).getBytes())).
                header("Content-Type", "application/json; charset=UTF-8")
                .body(JSON.toJSONString(map))
                .execute().body();

        System.out.println("result:[{}]" + result);

        JSONObject jsonObject = JSON.parseObject(result);
        System.out.println("获取phone:{}" + JSON.toJSON(jsonObject));
        String cryptograph_phone = (String) jsonObject.get("phone");

        if (StrUtil.isBlank(cryptograph_phone)) {
            throw new RuntimeException("极光认证出错");
        }
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(prikey));
        PrivateKey privateKey = KeyFactory.getInstance("RSA").generatePrivate(keySpec);

        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.DECRYPT_MODE, privateKey);

        byte[] b = Base64.getDecoder().decode(cryptograph_phone);

        return new String(cipher.doFinal(b));
    }
}
