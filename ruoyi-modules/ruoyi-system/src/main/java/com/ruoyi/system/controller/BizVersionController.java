package com.ruoyi.system.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.system.domain.BizVersion;
import com.ruoyi.system.service.IBizVersionService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 版本Controller
 * 
 * @author ruoyi
 * @date 2021-12-14
 */
@RestController
@RequestMapping("/version")
public class BizVersionController extends BaseController
{
    @Autowired
    private IBizVersionService bizVersionService;

    /**
     * 查询版本列表
     */
    @GetMapping("/list")
    public AjaxResult list(BizVersion bizVersion)
    {
        return AjaxResult.success(bizVersionService.selectBizVersionList(bizVersion));
    }


    /**
     * 查询版本列表
     */
    @RequiresPermissions("system:version:list")
    @GetMapping("/query")
    public TableDataInfo query(BizVersion bizVersion)
    {
        startPage();
        List<BizVersion> list = bizVersionService.selectBizVersionList(bizVersion);
        return getDataTable(list);
    }

    /**
     * 导出版本列表
     */
    @RequiresPermissions("system:version:export")
    @Log(title = "版本", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BizVersion bizVersion)
    {
        List<BizVersion> list = bizVersionService.selectBizVersionList(bizVersion);
        ExcelUtil<BizVersion> util = new ExcelUtil<BizVersion>(BizVersion.class);
        util.exportExcel(response, list, "版本数据");
    }

    /**
     * 获取版本详细信息
     */
    @RequiresPermissions("system:version:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(bizVersionService.selectBizVersionById(id));
    }

    /**
     * 新增版本
     */
    @RequiresPermissions("system:version:add")
    @Log(title = "版本", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BizVersion bizVersion)
    {
        return toAjax(bizVersionService.insertBizVersion(bizVersion));
    }

    /**
     * 修改版本
     */
    @RequiresPermissions("system:version:edit")
    @Log(title = "版本", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BizVersion bizVersion)
    {
        return toAjax(bizVersionService.updateBizVersion(bizVersion));
    }

    /**
     * 删除版本
     */
    @RequiresPermissions("system:version:remove")
    @Log(title = "版本", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(bizVersionService.deleteBizVersionByIds(ids));
    }
}
