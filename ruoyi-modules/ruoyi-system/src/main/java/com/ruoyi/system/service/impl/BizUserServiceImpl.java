package com.ruoyi.system.service.impl;

import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.system.domain.BizUser;
import com.ruoyi.system.mapper.BizUserMapper;
import com.ruoyi.system.service.IBizUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 用户Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-02-06
 */
@Service
public class BizUserServiceImpl implements IBizUserService
{
    @Autowired
    private BizUserMapper bizUserMapper;

    /**
     * 查询用户
     * 
     * @param id 用户ID
     * @return 用户
     */
    @Override
    public BizUser selectBizUserById(Long id)
    {
        return bizUserMapper.selectBizUserById(id);
    }

    public BizUser selectBizUserByMobile(String mobile) {
        return bizUserMapper.selectBizUserByMobile(mobile);
    }
    /**
     * 查询用户列表
     * 
     * @param bizUser 用户
     * @return 用户
     */
    @Override
    public List<BizUser> selectBizUserList(BizUser bizUser)
    {
        return bizUserMapper.selectBizUserList(bizUser);
    }

    /**
     * 新增用户
     * 
     * @param bizUser 用户
     * @return 结果
     */
    @Override
    public int insertBizUser(BizUser bizUser)
    {
        bizUser.setCreateTime(DateUtils.getNowDate());
        return bizUserMapper.insertBizUser(bizUser);
    }

    /**
     * 修改用户
     * 
     * @param bizUser 用户
     * @return 结果
     */
    @Override
    public int updateBizUser(BizUser bizUser)
    {
        bizUser.setUpdateTime(DateUtils.getNowDate());
        return bizUserMapper.updateBizUser(bizUser);
    }

    /**
     * 删除用户对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteBizUserByIds(String ids)
    {
        return bizUserMapper.deleteBizUserByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除用户信息
     * 
     * @param id 用户ID
     * @return 结果
     */
    public int deleteBizUserById(Long id)
    {
        return bizUserMapper.deleteBizUserById(id);
    }
}
