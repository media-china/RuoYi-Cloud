package com.ruoyi.system.controller;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.system.domain.BizInvite;
import com.ruoyi.system.domain.BizRelation;
import com.ruoyi.system.service.IBizInviteService;
import com.ruoyi.system.service.IBizRelationService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 邀请 提供者
 * 
 * @author ruoyi
 * @date 2020-02-06
 */
@RestController
@RequestMapping("invite")
@Api("邀请")
public class BizInviteController extends BaseController
{
	
	@Autowired
	private IBizInviteService bizInviteService;

	@Autowired
	private IBizRelationService bizRelationService;
	
	/**
	 * 查询${tableComment}
	 */
	@GetMapping("get/{id}")
	public BizInvite get(@PathVariable("id") Long id)
	{
		return bizInviteService.selectBizInviteById(id);
		
	}
	
	/**
	 * 查询邀请列表
	 */
	@RequestMapping("list")
	@ResponseBody
	public R list(BizInvite bizInvite)
	{
		startPage();
        return R.ok(bizInviteService.selectBizInviteList(bizInvite));
	}
	
	
	/**
	 * 新增保存邀请
	 */
	@PostMapping("save")
	public R addSave(@RequestBody BizInvite bizInvite)
	{		
		return R.ok(bizInviteService.insertBizInvite(bizInvite));
	}

	/**
	 * 修改保存邀请
	 */
	@PostMapping("update")
	public R editSave(@RequestBody BizInvite bizInvite)
	{		
		return R.ok(bizInviteService.updateBizInvite(bizInvite));
	}
	
	/**
	 * 删除${tableComment}
	 */
	@PostMapping("remove")
	public R remove(String ids)
	{		
		return R.ok(bizInviteService.deleteBizInviteByIds(ids));
	}

	@RequestMapping("add")
	public R add(String myMobile, String youMobile) {

		BizInvite invite = new BizInvite();
		// 实际情况应该用id，因为可能修改手机号，先埋个雷
		myMobile = myMobile.trim();
		youMobile = youMobile.trim();
		invite.setMyMobile(myMobile);
		invite.setYouMobile(youMobile);
		List<BizInvite> inviteList = bizInviteService.selectBizInviteList(invite);
		if(inviteList == null || inviteList.isEmpty()) {
			invite.setStatus("0");
			bizInviteService.insertBizInvite(invite);
		}

		return R.ok();
	}

	@RequestMapping("updateStatus")
	public R updateStatus(String myMobile, String youMobile, String status) {

		BizInvite invite = new BizInvite();
		invite.setMyMobile(myMobile);
		invite.setYouMobile(youMobile);
		invite.setStatus(status);
		bizInviteService.updateBizInvite(invite);

		BizRelation relation1 = new BizRelation();
		relation1.setMyMobile(myMobile);
		relation1.setYouMobile(youMobile);
		List<BizRelation> relations1 = bizRelationService.selectBizRelationList(relation1);
		if(relations1 == null || relations1.isEmpty()) {
			bizRelationService.insertBizRelation(relation1);
		}

		BizRelation relation2 = new BizRelation();
		relation2.setMyMobile(youMobile);
		relation2.setYouMobile(myMobile);
		List<BizRelation> relations2 = bizRelationService.selectBizRelationList(relation2);
		if(relations2 == null || relations2.isEmpty()) {
			bizRelationService.insertBizRelation(relation2);
		}

		return R.ok();
	}
	
}
