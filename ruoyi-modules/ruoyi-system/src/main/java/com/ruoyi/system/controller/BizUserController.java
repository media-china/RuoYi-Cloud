package com.ruoyi.system.controller;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.system.domain.BizUser;
import com.ruoyi.system.service.IBizUserService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 用户 提供者
 * 
 * @author ruoyi
 * @date 2020-02-06
 */
@RestController
//@RequestMapping("user")
//因为跟系统用户冲突，改为member
@RequestMapping("member")
@Api("用户")
public class BizUserController extends BaseController
{
	
	@Autowired
	private IBizUserService bizUserService;
	
	/**
	 * 查询${tableComment}
	 */
	@GetMapping("get/{id}")
	public BizUser get(@PathVariable("id") Long id)
	{
		return bizUserService.selectBizUserById(id);
		
	}
	
	/**
	 * 查询用户列表
	 */
	@GetMapping("list")
	public AjaxResult list(BizUser bizUser)
	{
		startPage();
        return AjaxResult.success(bizUserService.selectBizUserList(bizUser));
	}
	
	
	/**
	 * 新增保存用户
	 */
	@RequestMapping("save")
	public AjaxResult addSave(@RequestBody BizUser bizUser)
	{		
		return toAjax(bizUserService.insertBizUser(bizUser));
	}

	/**
	 * 修改保存用户
	 */
	@RequestMapping("update")
	public AjaxResult editSave(@RequestBody BizUser bizUser)
	{		
		return toAjax(bizUserService.updateBizUser(bizUser));
	}
	
	/**
	 * 删除${tableComment}
	 */
	@RequestMapping("remove")
	public AjaxResult remove(String ids)
	{		
		return toAjax(bizUserService.deleteBizUserByIds(ids));
	}

	@RequestMapping("updateLocation")
	public R updateLocation(String mobile, String x, String y) {

		BizUser bizUser = new BizUser();
		bizUser.setMobile(mobile);
		List<BizUser> users = bizUserService.selectBizUserList(bizUser);

		if(users != null && users.size() > 0) {
			BizUser user = users.get(0);
			user.setX(x);
			user.setY(y);
			bizUserService.updateBizUser(user);
			return R.ok();
		}

		return R.fail("哦哟，用户好像没注册！");
	}

	/**
	 * 注册和登陆都用通一个接口
	 */
	@RequestMapping("login")
	public R login(String mobile) {

		BizUser bizUser = new BizUser();
		bizUser.setMobile(mobile);
		List<BizUser> users = bizUserService.selectBizUserList(bizUser);
		Map<String, Object> userMap = new HashMap<String, Object>();
		if(users != null && users.size() > 0) {
			// 用户存在则返回用户信息
			userMap.put("userInfo", users.get(0));
		} else {
			bizUserService.insertBizUser(bizUser);
			userMap.put("userInfo", bizUser);
		}

		return R.ok(userMap);
	}

	/**
	 * 注册和登陆都用通一个接口
	 */
	@RequestMapping("query")
	public R query(String mobile) {

		BizUser bizUser = new BizUser();
		bizUser.setMobile(mobile);
		List<BizUser> users = bizUserService.selectBizUserList(bizUser);
		if(users != null && users.size() > 0) {
			// 用户存在则返回用户信息
			return R.ok(users.get(0));
		}

		return R.ok();
	}
}
