package com.ruoyi.system.service;

import com.ruoyi.system.domain.BizRelation;

import java.util.List;

/**
 * 用户关系Service接口
 * 
 * @author ruoyi
 * @date 2020-02-06
 */
public interface IBizRelationService 
{
    /**
     * 查询用户关系
     * 
     * @param id 用户关系ID
     * @return 用户关系
     */
    public BizRelation selectBizRelationById(Long id);

    /**
     * 查询用户关系列表
     * 
     * @param bizRelation 用户关系
     * @return 用户关系集合
     */
    public List<BizRelation> selectBizRelationList(BizRelation bizRelation);

    /**
     * 新增用户关系
     * 
     * @param bizRelation 用户关系
     * @return 结果
     */
    public int insertBizRelation(BizRelation bizRelation);

    /**
     * 修改用户关系
     * 
     * @param bizRelation 用户关系
     * @return 结果
     */
    public int updateBizRelation(BizRelation bizRelation);

    /**
     * 批量删除用户关系
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBizRelationByIds(String ids);

    /**
     * 删除用户关系信息
     * 
     * @param id 用户关系ID
     * @return 结果
     */
    public int deleteBizRelationById(Long id);
}
