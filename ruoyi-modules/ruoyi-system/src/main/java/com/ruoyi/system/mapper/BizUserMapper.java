package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.BizUser;

import java.util.List;

/**
 * 用户Mapper接口
 * 
 * @author ruoyi
 * @date 2020-02-06
 */
public interface BizUserMapper 
{
    /**
     * 查询用户
     *
     * @param id 用户ID
     * @return 用户
     */
    public BizUser selectBizUserById(Long id);


    /**
     * 查询用户
     *
     * @param id 用户ID
     * @return 用户
     */
    public BizUser selectBizUserByMobile(String mobile);

    /**
     * 查询用户列表
     * 
     * @param bizUser 用户
     * @return 用户集合
     */
    public List<BizUser> selectBizUserList(BizUser bizUser);

    /**
     * 新增用户
     * 
     * @param bizUser 用户
     * @return 结果
     */
    public int insertBizUser(BizUser bizUser);

    /**
     * 修改用户
     * 
     * @param bizUser 用户
     * @return 结果
     */
    public int updateBizUser(BizUser bizUser);

    /**
     * 删除用户
     * 
     * @param id 用户ID
     * @return 结果
     */
    public int deleteBizUserById(Long id);

    /**
     * 批量删除用户
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBizUserByIds(String[] ids);
}
