package com.ruoyi.system.service.impl;

import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.system.domain.BizInvite;
import com.ruoyi.system.mapper.BizInviteMapper;
import com.ruoyi.system.service.IBizInviteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 邀请Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-02-06
 */
@Service
public class BizInviteServiceImpl implements IBizInviteService
{
    @Autowired
    private BizInviteMapper bizInviteMapper;

    /**
     * 查询邀请
     * 
     * @param id 邀请ID
     * @return 邀请
     */
    @Override
    public BizInvite selectBizInviteById(Long id)
    {
        return bizInviteMapper.selectBizInviteById(id);
    }

    /**
     * 查询邀请列表
     * 
     * @param bizInvite 邀请
     * @return 邀请
     */
    @Override
    public List<BizInvite> selectBizInviteList(BizInvite bizInvite)
    {
        return bizInviteMapper.selectBizInviteList(bizInvite);
    }

    /**
     * 新增邀请
     * 
     * @param bizInvite 邀请
     * @return 结果
     */
    @Override
    public int insertBizInvite(BizInvite bizInvite)
    {
        bizInvite.setCreateTime(DateUtils.getNowDate());
        return bizInviteMapper.insertBizInvite(bizInvite);
    }

    /**
     * 修改邀请
     * 
     * @param bizInvite 邀请
     * @return 结果
     */
    @Override
    public int updateBizInvite(BizInvite bizInvite)
    {
        bizInvite.setUpdateTime(DateUtils.getNowDate());
        return bizInviteMapper.updateBizInvite(bizInvite);
    }

    /**
     * 删除邀请对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteBizInviteByIds(String ids)
    {
        return bizInviteMapper.deleteBizInviteByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除邀请信息
     * 
     * @param id 邀请ID
     * @return 结果
     */
    public int deleteBizInviteById(Long id)
    {
        return bizInviteMapper.deleteBizInviteById(id);
    }
}
