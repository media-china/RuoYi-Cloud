package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.BizVersionMapper;
import com.ruoyi.system.domain.BizVersion;
import com.ruoyi.system.service.IBizVersionService;

/**
 * 版本Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-12-14
 */
@Service
public class BizVersionServiceImpl implements IBizVersionService 
{
    @Autowired
    private BizVersionMapper bizVersionMapper;

    /**
     * 查询版本
     * 
     * @param id 版本主键
     * @return 版本
     */
    @Override
    public BizVersion selectBizVersionById(Long id)
    {
        return bizVersionMapper.selectBizVersionById(id);
    }

    /**
     * 查询版本列表
     * 
     * @param bizVersion 版本
     * @return 版本
     */
    @Override
    public List<BizVersion> selectBizVersionList(BizVersion bizVersion)
    {
        return bizVersionMapper.selectBizVersionList(bizVersion);
    }

    /**
     * 新增版本
     * 
     * @param bizVersion 版本
     * @return 结果
     */
    @Override
    public int insertBizVersion(BizVersion bizVersion)
    {
        bizVersion.setCreateTime(DateUtils.getNowDate());
        return bizVersionMapper.insertBizVersion(bizVersion);
    }

    /**
     * 修改版本
     * 
     * @param bizVersion 版本
     * @return 结果
     */
    @Override
    public int updateBizVersion(BizVersion bizVersion)
    {
        bizVersion.setUpdateTime(DateUtils.getNowDate());
        return bizVersionMapper.updateBizVersion(bizVersion);
    }

    /**
     * 批量删除版本
     * 
     * @param ids 需要删除的版本主键
     * @return 结果
     */
    @Override
    public int deleteBizVersionByIds(Long[] ids)
    {
        return bizVersionMapper.deleteBizVersionByIds(ids);
    }

    /**
     * 删除版本信息
     * 
     * @param id 版本主键
     * @return 结果
     */
    @Override
    public int deleteBizVersionById(Long id)
    {
        return bizVersionMapper.deleteBizVersionById(id);
    }
}
