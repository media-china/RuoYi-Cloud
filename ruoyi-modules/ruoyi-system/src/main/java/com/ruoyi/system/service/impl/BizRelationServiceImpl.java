package com.ruoyi.system.service.impl;

import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.system.domain.BizRelation;
import com.ruoyi.system.mapper.BizRelationMapper;
import com.ruoyi.system.service.IBizRelationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 用户关系Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-02-06
 */
@Service
public class BizRelationServiceImpl implements IBizRelationService
{
    @Autowired
    private BizRelationMapper bizRelationMapper;

    /**
     * 查询用户关系
     * 
     * @param id 用户关系ID
     * @return 用户关系
     */
    @Override
    public BizRelation selectBizRelationById(Long id)
    {
        return bizRelationMapper.selectBizRelationById(id);
    }

    /**
     * 查询用户关系列表
     * 
     * @param bizRelation 用户关系
     * @return 用户关系
     */
    @Override
    public List<BizRelation> selectBizRelationList(BizRelation bizRelation)
    {
        return bizRelationMapper.selectBizRelationList(bizRelation);
    }

    /**
     * 新增用户关系
     * 
     * @param bizRelation 用户关系
     * @return 结果
     */
    @Override
    public int insertBizRelation(BizRelation bizRelation)
    {
        bizRelation.setCreateTime(DateUtils.getNowDate());
        return bizRelationMapper.insertBizRelation(bizRelation);
    }

    /**
     * 修改用户关系
     * 
     * @param bizRelation 用户关系
     * @return 结果
     */
    @Override
    public int updateBizRelation(BizRelation bizRelation)
    {
        bizRelation.setUpdateTime(DateUtils.getNowDate());
        return bizRelationMapper.updateBizRelation(bizRelation);
    }

    /**
     * 删除用户关系对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteBizRelationByIds(String ids)
    {
        return bizRelationMapper.deleteBizRelationByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除用户关系信息
     * 
     * @param id 用户关系ID
     * @return 结果
     */
    public int deleteBizRelationById(Long id)
    {
        return bizRelationMapper.deleteBizRelationById(id);
    }
}
