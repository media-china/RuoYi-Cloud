package com.ruoyi.system.controller;

import com.ruoyi.system.domain.BizUser;
import com.ruoyi.system.service.IBizUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/location")
public class BizLocationController {

    @Autowired
    IBizUserService bizUserService;

    @RequestMapping("/share")
    public String share(Model model, String mobile) {

        BizUser user = bizUserService.selectBizUserByMobile(mobile);
        model.addAttribute("x", user.getX());
        model.addAttribute("y", user.getY());
        model.addAttribute("user", user);

        return "location/share";
    }

    @RequestMapping("/info")
    @ResponseBody
    public BizUser info(Model model, String mobile) {

        BizUser user = bizUserService.selectBizUserByMobile(mobile);

        return user;
    }
}
