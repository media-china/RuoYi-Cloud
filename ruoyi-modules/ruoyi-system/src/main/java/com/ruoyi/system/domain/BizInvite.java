package com.ruoyi.system.domain;

import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 邀请对象 biz_invite
 * 
 * @author ruoyi
 * @date 2020-07-12
 */
public class BizInvite extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long id;

    /** 邀请用户 */
    @Excel(name = "邀请用户")
    private Long myId;

    /** 邀请手机号 */
    @Excel(name = "邀请手机号")
    private String myMobile;

    /** 被邀请用户 */
    @Excel(name = "被邀请用户")
    private Long youId;

    /** 被邀请手机号 */
    @Excel(name = "被邀请手机号")
    private String youMobile;

    /** 状态（0待接受 1同意 2拒绝） */
    @Excel(name = "状态", readConverterExp = "0=待接受,1=同意,2=拒绝")
    private String status;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setMyId(Long myId) 
    {
        this.myId = myId;
    }

    public Long getMyId() 
    {
        return myId;
    }
    public void setMyMobile(String myMobile) 
    {
        this.myMobile = myMobile;
    }

    public String getMyMobile() 
    {
        return myMobile;
    }
    public void setYouId(Long youId) 
    {
        this.youId = youId;
    }

    public Long getYouId() 
    {
        return youId;
    }
    public void setYouMobile(String youMobile) 
    {
        this.youMobile = youMobile;
    }

    public String getYouMobile() 
    {
        return youMobile;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("myId", getMyId())
            .append("myMobile", getMyMobile())
            .append("youId", getYouId())
            .append("youMobile", getYouMobile())
            .append("status", getStatus())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
