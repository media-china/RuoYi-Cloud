package com.ruoyi.system.domain;

import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 用户关系对象 biz_relation
 * 
 * @author ruoyi
 * @date 2020-07-12
 */
public class BizRelation extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long id;

    /** 用户 */
    @Excel(name = "用户")
    private Long myId;

    /** 手机号 */
    @Excel(name = "手机号")
    private String myMobile;

    /** 联系人用户 */
    @Excel(name = "联系人用户")
    private Long youId;

    /** 联系人手机号 */
    @Excel(name = "联系人手机号")
    private String youMobile;

    /** 关系类型（0亲人 1朋友） */
    @Excel(name = "关系类型", readConverterExp = "0=亲人,1=朋友")
    private String type;

    /** 状态（0正常 1消息免打扰 2拉黑） */
    @Excel(name = "状态", readConverterExp = "0=正常,1=消息免打扰,2=拉黑")
    private String status;

    /** 位置状态（0正常 1对其隐身 2靠近消失） */
    @Excel(name = "位置状态", readConverterExp = "0=正常,1=对其隐身,2=靠近消失")
    private String locateStatus;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setMyId(Long myId) 
    {
        this.myId = myId;
    }

    public Long getMyId() 
    {
        return myId;
    }
    public void setMyMobile(String myMobile) 
    {
        this.myMobile = myMobile;
    }

    public String getMyMobile() 
    {
        return myMobile;
    }
    public void setYouId(Long youId) 
    {
        this.youId = youId;
    }

    public Long getYouId() 
    {
        return youId;
    }
    public void setYouMobile(String youMobile) 
    {
        this.youMobile = youMobile;
    }

    public String getYouMobile() 
    {
        return youMobile;
    }
    public void setType(String type) 
    {
        this.type = type;
    }

    public String getType() 
    {
        return type;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setLocateStatus(String locateStatus) 
    {
        this.locateStatus = locateStatus;
    }

    public String getLocateStatus() 
    {
        return locateStatus;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("myId", getMyId())
            .append("myMobile", getMyMobile())
            .append("youId", getYouId())
            .append("youMobile", getYouMobile())
            .append("type", getType())
            .append("status", getStatus())
            .append("locateStatus", getLocateStatus())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
