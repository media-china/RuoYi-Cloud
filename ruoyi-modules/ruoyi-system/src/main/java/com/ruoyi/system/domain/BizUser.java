package com.ruoyi.system.domain;

import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 用户对象 biz_user
 * 
 * @author ruoyi
 * @date 2020-02-06
 */
public class BizUser extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long id;

    /** 手机号 */
    @Excel(name = "手机号")
    private String mobile;

    /** 验证码 */
    @Excel(name = "验证码")
    private String code;

    /** 密码 */
    @Excel(name = "密码")
    private String password;

    /** 盐 */
    @Excel(name = "盐")
    private String salt;

    /** openid */
    @Excel(name = "openid")
    private String openid;

    /** 昵称 */
    @Excel(name = "昵称")
    private String nickName;

    /** 纬度 */
    @Excel(name = "纬度")
    private String x;

    /** 经度 */
    @Excel(name = "经度")
    private String y;

    /** token */
    @Excel(name = "token")
    private String token;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setMobile(String mobile) 
    {
        this.mobile = mobile;
    }

    public String getMobile() 
    {
        return mobile;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setPassword(String password) 
    {
        this.password = password;
    }

    public String getPassword() 
    {
        return password;
    }
    public void setSalt(String salt) 
    {
        this.salt = salt;
    }

    public String getSalt() 
    {
        return salt;
    }
    public void setOpenid(String openid) 
    {
        this.openid = openid;
    }

    public String getOpenid() 
    {
        return openid;
    }
    public void setNickName(String nickName) 
    {
        this.nickName = nickName;
    }

    public String getNickName() 
    {
        return nickName;
    }
    public void setX(String x) 
    {
        this.x = x;
    }

    public String getX() 
    {
        return x;
    }
    public void setY(String y) 
    {
        this.y = y;
    }

    public String getY() 
    {
        return y;
    }
    public void setToken(String token) 
    {
        this.token = token;
    }

    public String getToken() 
    {
        return token;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("mobile", getMobile())
            .append("code", getCode())
            .append("password", getPassword())
            .append("salt", getSalt())
            .append("openid", getOpenid())
            .append("nickName", getNickName())
            .append("x", getX())
            .append("y", getY())
            .append("token", getToken())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
