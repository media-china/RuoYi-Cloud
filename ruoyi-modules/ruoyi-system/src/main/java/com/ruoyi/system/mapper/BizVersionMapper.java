package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.BizVersion;

/**
 * 版本Mapper接口
 * 
 * @author ruoyi
 * @date 2021-12-14
 */
public interface BizVersionMapper 
{
    /**
     * 查询版本
     * 
     * @param id 版本主键
     * @return 版本
     */
    public BizVersion selectBizVersionById(Long id);

    /**
     * 查询版本列表
     * 
     * @param bizVersion 版本
     * @return 版本集合
     */
    public List<BizVersion> selectBizVersionList(BizVersion bizVersion);

    /**
     * 新增版本
     * 
     * @param bizVersion 版本
     * @return 结果
     */
    public int insertBizVersion(BizVersion bizVersion);

    /**
     * 修改版本
     * 
     * @param bizVersion 版本
     * @return 结果
     */
    public int updateBizVersion(BizVersion bizVersion);

    /**
     * 删除版本
     * 
     * @param id 版本主键
     * @return 结果
     */
    public int deleteBizVersionById(Long id);

    /**
     * 批量删除版本
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBizVersionByIds(Long[] ids);
}
