package com.ruoyi.system.service;

import com.ruoyi.system.domain.BizInvite;

import java.util.List;

/**
 * 邀请Service接口
 * 
 * @author ruoyi
 * @date 2020-02-06
 */
public interface IBizInviteService 
{
    /**
     * 查询邀请
     * 
     * @param id 邀请ID
     * @return 邀请
     */
    public BizInvite selectBizInviteById(Long id);

    /**
     * 查询邀请列表
     * 
     * @param bizInvite 邀请
     * @return 邀请集合
     */
    public List<BizInvite> selectBizInviteList(BizInvite bizInvite);

    /**
     * 新增邀请
     * 
     * @param bizInvite 邀请
     * @return 结果
     */
    public int insertBizInvite(BizInvite bizInvite);

    /**
     * 修改邀请
     * 
     * @param bizInvite 邀请
     * @return 结果
     */
    public int updateBizInvite(BizInvite bizInvite);

    /**
     * 批量删除邀请
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBizInviteByIds(String ids);

    /**
     * 删除邀请信息
     * 
     * @param id 邀请ID
     * @return 结果
     */
    public int deleteBizInviteById(Long id);
}
