package com.ruoyi.system.controller;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.system.domain.BizRelation;
import com.ruoyi.system.service.IBizRelationService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 用户关系 提供者
 * 
 * @author ruoyi
 * @date 2020-02-06
 */
@RestController
@RequestMapping("relation")
@Api("关系")
public class BizRelationController extends BaseController
{
	
	@Autowired
	private IBizRelationService bizRelationService;
	
	/**
	 * 查询${tableComment}
	 */
	@GetMapping("get/{id}")
	public BizRelation get(@PathVariable("id") Long id)
	{
		return bizRelationService.selectBizRelationById(id);
		
	}
	
	/**
	 * 查询用户关系列表
	 */
	@GetMapping("list")
	public R list(BizRelation bizRelation)
	{
		startPage();
        return R.ok(bizRelationService.selectBizRelationList(bizRelation));
	}

	@RequestMapping("mList")
	@ResponseBody
	public Object mList(String myMobile) {

		BizRelation relation = new BizRelation();
		relation.setMyMobile(myMobile);
		List<BizRelation> relations = bizRelationService.selectBizRelationList(relation);

		return R.ok(relations, "relations");
	}
	
	/**
	 * 新增保存用户关系
	 */
	@PostMapping("save")
	public R addSave(@RequestBody BizRelation bizRelation)
	{		
		return R.ok(bizRelationService.insertBizRelation(bizRelation));
	}

	/**
	 * 修改保存用户关系
	 */
	@PostMapping("update")
	public R editSave(@RequestBody BizRelation bizRelation)
	{		
		return R.ok(bizRelationService.updateBizRelation(bizRelation));
	}
	
	/**
	 * 删除${tableComment}
	 */
	@PostMapping("remove")
	public R remove(String ids)
	{		
		return R.ok(bizRelationService.deleteBizRelationByIds(ids));
	}
	
}
