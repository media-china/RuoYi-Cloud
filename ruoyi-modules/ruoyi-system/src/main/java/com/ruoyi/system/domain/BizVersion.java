package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 版本对象 biz_version
 * 
 * @author ruoyi
 * @date 2021-12-14
 */
public class BizVersion extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long id;

    /** 应用名称 */
    @Excel(name = "应用名称")
    private String appName;

    /** 包名 */
    @Excel(name = "包名")
    private String appPackage;

    /** 设备类型（00安卓 01iOS 02Windows 03Mac） */
    @Excel(name = "设备类型", readConverterExp = "0=0安卓,0=1iOS,0=2Windows,0=3Mac")
    private String deviceType;

    /** 版本号 */
    @Excel(name = "版本号")
    private String version;

    /** 描述 */
    @Excel(name = "描述")
    private String description;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setAppName(String appName) 
    {
        this.appName = appName;
    }

    public String getAppName() 
    {
        return appName;
    }
    public void setAppPackage(String appPackage) 
    {
        this.appPackage = appPackage;
    }

    public String getAppPackage() 
    {
        return appPackage;
    }
    public void setDeviceType(String deviceType) 
    {
        this.deviceType = deviceType;
    }

    public String getDeviceType() 
    {
        return deviceType;
    }
    public void setVersion(String version) 
    {
        this.version = version;
    }

    public String getVersion() 
    {
        return version;
    }
    public void setDescription(String description) 
    {
        this.description = description;
    }

    public String getDescription() 
    {
        return description;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("appName", getAppName())
            .append("appPackage", getAppPackage())
            .append("deviceType", getDeviceType())
            .append("version", getVersion())
            .append("description", getDescription())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
