package com.ruoyi.file.service;

import java.util.List;
import com.ruoyi.file.domain.SysFile;

/**
 * 文件Service接口
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
public interface ISysFileService 
{
    /**
     * 查询文件
     * 
     * @param id 文件主键
     * @return 文件
     */
    public SysFile selectSysFileById(Long id);

    /**
     * 查询文件列表
     * 
     * @param sysFile 文件
     * @return 文件集合
     */
    public List<SysFile> selectSysFileList(SysFile sysFile);

    /**
     * 新增文件
     * 
     * @param sysFile 文件
     * @return 结果
     */
    public int insertSysFile(SysFile sysFile);

    /**
     * 修改文件
     * 
     * @param sysFile 文件
     * @return 结果
     */
    public int updateSysFile(SysFile sysFile);

    /**
     * 批量删除文件
     * 
     * @param ids 需要删除的文件主键集合
     * @return 结果
     */
    public int deleteSysFileByIds(Long[] ids);

    /**
     * 删除文件信息
     * 
     * @param id 文件主键
     * @return 结果
     */
    public int deleteSysFileById(Long id);
}
