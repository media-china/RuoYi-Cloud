package com.ruoyi.file.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 文件对象 sys_file
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
public class SysFile extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 文件id */
    private Long id;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 地址 */
    @Excel(name = "地址")
    private String url;

    /** 描述 */
    @Excel(name = "描述")
    private String description;

    /** 首页视频id */
    @Excel(name = "首页视频id")
    private Long parentId;

    /** 序号 */
    @Excel(name = "序号")
    private Long Order;

    /** 是否vip（0是 1否） */
    @Excel(name = "是否vip", readConverterExp = "0=是,1=否")
    private Long isVip;

    /** 所属用户 */
    @Excel(name = "所属用户")
    private Long userId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setUrl(String url) 
    {
        this.url = url;
    }

    public String getUrl() 
    {
        return url;
    }
    public void setDescription(String description) 
    {
        this.description = description;
    }

    public String getDescription() 
    {
        return description;
    }
    public void setParentId(Long parentId) 
    {
        this.parentId = parentId;
    }

    public Long getParentId() 
    {
        return parentId;
    }
    public void setOrder(Long Order) 
    {
        this.Order = Order;
    }

    public Long getOrder() 
    {
        return Order;
    }
    public void setIsVip(Long isVip) 
    {
        this.isVip = isVip;
    }

    public Long getIsVip() 
    {
        return isVip;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("url", getUrl())
            .append("description", getDescription())
            .append("parentId", getParentId())
            .append("Order", getOrder())
            .append("isVip", getIsVip())
            .append("userId", getUserId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
