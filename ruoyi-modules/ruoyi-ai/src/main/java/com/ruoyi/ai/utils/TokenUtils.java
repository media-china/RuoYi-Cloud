package com.ruoyi.ai.utils;

import com.ruoyi.common.core.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

/**
 * 参考文档：https://ai.baidu.com/ai-doc/REFERENCE/Ck3dwjhhu
 */
@Component
public class TokenUtils {

    @Autowired
    private RedisTemplate redisTemplate;

    private static final String ACCESS_TOKEN_URL = "https://aip.baidubce.com/oauth/2.0/token";

    private static final String grant_type = "client_credentials";
    private static final String client_id = "Xn5GX6oDUwGTlVyCGAMaMwGp";
    private static final String client_secret = "loN2k7LlzNnAcgyboZvX56zzUbYCHeAX";

    public String getAccessToken() {

        String access_token = (String) redisTemplate.opsForValue().get("baidu_access_token");
        if(StringUtils.isEmpty(access_token)) {

            RestTemplate restTemplate = new RestTemplate();

            Map<String, String> request = new HashMap<String, String>();

            request.put("grant_type", grant_type);
            request.put("client_id", client_id);
            request.put("client_secret", client_secret);

            Map<String, String> response = restTemplate.postForObject(ACCESS_TOKEN_URL + "?grant_type=" + grant_type + "&client_id=" + client_id + "&client_secret=" + client_secret, null, Map.class);

            access_token = response.get("access_token");

            if(StringUtils.isNotEmpty(access_token)) {
                int expires = 2505600;
                redisTemplate.opsForValue().set("baidu_access_token",access_token,expires);
            }
        }

        return access_token;
    }

    public static void main(String[] args) {
//        System.out.println(getAccessToken());
    }
}
