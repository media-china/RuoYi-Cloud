package com.ruoyi.ai.controller;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.ai.bo.Message;
import com.ruoyi.common.core.utils.StringUtils;
import okhttp3.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("gpt")
public class GptController {

    @Value("${ernie-bot.client_id}")
    private String clientId;

    @Value("${ernie-bot.client_secret}")
    private String clientSecret;

    private final String ERNIE_BOT_ACCESS_TOKEN = "ernie_token";

    private final String ACCESS_TOKEN_LOCK = "token_lock";

    @Autowired
    private RedisTemplate redisTemplate;

    static final OkHttpClient HTTP_CLIENT = new OkHttpClient().newBuilder().build();

    /**
     * 先获取token
     * 这里咱们用一个分布式锁来实现下看看
     */
    public String getAccessToken() throws IOException, InterruptedException {
        // 先从redis获取，如果没有再到官方获取
        String accessToken = (String) redisTemplate.opsForValue().get(ERNIE_BOT_ACCESS_TOKEN);
        if(StringUtils.isNotEmpty(accessToken)) {
            return accessToken;
        }

        // 1. 先尝试加锁
        long tid = Thread.currentThread().getId();
        Thread delayThread = null;
        try {
            while(Boolean.FALSE.equals(redisTemplate.opsForValue().setIfAbsent(ACCESS_TOKEN_LOCK, tid, 30, TimeUnit.SECONDS))) {
                Thread.sleep(1000);
            }
            // 2. 拿到锁之后需要对锁进行续命
            // 写一个定时任务的线程
            delayThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    while(true) {
                        try {
                            Thread.sleep(15 * 1000 * 1000);
                            long tidValue = (long) redisTemplate.opsForValue().get(ACCESS_TOKEN_LOCK);
                            if(Objects.equals(tid, tidValue)) {
                                redisTemplate.opsForValue().getAndExpire(ACCESS_TOKEN_LOCK, 30, TimeUnit.SECONDS);
                            }
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
            delayThread.start();
            delayThread.join();

            // 3. 执行相关业务
            // 调用百度的接口
            synchronized (this) {
                MediaType mediaType = MediaType.parse("application/json");
                RequestBody body = RequestBody.create(mediaType, "");
                Request request = new Request.Builder()
                        .url("https://aip.baidubce.com/oauth/2.0/token?client_id=" + clientId + "&client_secret=" + clientSecret + "&grant_type=client_credentials")
                        .method("POST", body)
                        .addHeader("Content-Type", "application/json")
                        .addHeader("Accept", "application/json")
                        .build();
                Response response = HTTP_CLIENT.newCall(request).execute();
                JSONObject respBody = (JSONObject) JSONObject.parse(response.body().string());
                accessToken = (String) respBody.get("access_token");
                System.out.println(accessToken);
                redisTemplate.opsForValue().set(ERNIE_BOT_ACCESS_TOKEN, accessToken, 30, TimeUnit.DAYS);

                return accessToken;
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            // 4. 释放锁，确保是自己加的锁
            if(delayThread != null) {
                delayThread.interrupt();
            }
            long tidValue = (long) redisTemplate.opsForValue().get(ACCESS_TOKEN_LOCK);
            if(Objects.equals(tid, tidValue)) {
                redisTemplate.opsForValue().getAndDelete(ACCESS_TOKEN_LOCK);
            }
        }
    }

    @GetMapping("chat")
    public String chat(String content) throws IOException, InterruptedException {

        MediaType mediaType = MediaType.parse("application/json");

        Message  message = new Message();
        message.setRole("user");
        message.setContent(content);
        List<Message> messages = new ArrayList<>();
        messages.add(message);
        Map<String, List<Message>> param = new HashMap<>();
        param.put("messages", messages);

        RequestBody body = RequestBody.create(mediaType, JSONObject.toJSONString(param));
        System.out.println(JSONObject.toJSONString(messages));
        String accessToken = getAccessToken();
        Request request = new Request.Builder()
                .url("https://aip.baidubce.com/rpc/2.0/ai_custom/v1/wenxinworkshop/chat/eb-instant?access_token=" + accessToken)
                .method("POST", body)
                .addHeader("Content-Type", "application/json")
                .addHeader("Accept", "application/json")
                .build();

        Response response = HTTP_CLIENT.newCall(request).execute();
        return response.body().string();
    }

}
