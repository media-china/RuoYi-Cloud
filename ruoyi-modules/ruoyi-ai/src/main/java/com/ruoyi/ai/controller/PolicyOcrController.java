package com.ruoyi.ai.controller;

import com.ruoyi.ai.utils.TokenUtils;
import com.ruoyi.common.core.web.domain.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 参考文档：https://cloud.baidu.com/doc/OCR/s/Wk3h7y0eb
 */
@RestController
@RequestMapping("policy")
public class PolicyOcrController {

    private static final String policy_ocr_url = "https://aip.baidubce.com/rest/2.0/ocr/v1/insurance_documents";

    @Autowired
    private TokenUtils tokenUtils;

    @RequestMapping("ocr")
    public AjaxResult ocr(HttpServletRequest request, @RequestPart(value = "file") MultipartFile file) {

        RestTemplate restTemplate = new RestTemplate();

        String accessToken = tokenUtils.getAccessToken();
        request.setAttribute("access_token", accessToken);

        Map<String, Object> response = restTemplate.postForObject(policy_ocr_url, request, null, Map.class);

        return AjaxResult.success(response);
    }
}
